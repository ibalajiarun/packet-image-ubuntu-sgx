FROM scratch
ADD rootfs.tar.gz /

MAINTAINER David Laube <dlaube@packet.net>
LABEL name="Ubuntu Canonical Base Image" \
    vendor="Ubuntu" \
    license="GPLv2" \
    build-date="20181217"

# Setup the environment
ENV DEBIAN_FRONTEND=noninteractive

# Unminimize system
RUN yes | unminimize

# Install packages
RUN apt-get -q update && \
    apt-get -y -qq upgrade && \
    apt-get -y -qq install \
		apt-transport-https \
		bash \
		bash-completion \
		bc \
		biosdevname \
		ca-certificates \
		cloud-init \
		cron \
		curl \
		dbus \
		dstat \
		ethstatus \
		file \
		fio \
		htop \
		ifenslave \
		ioping \
		iotop \
		iperf \
		iptables \
		iputils-ping \
		jq \
		less \
		locales \
		locate \
		lsb-release \
		lsof \
		make \
		man-db \
		mdadm \
		mg \
		mosh \
		mtr-tiny \
		multipath-tools \
		nano \
		net-tools \
		netcat \
		nmap \
		ntp \
		ntpdate \
		open-iscsi \
		python-apt \
		python-yaml \
		rsync \
		rsyslog \
		screen \
		shunit2 \
		socat \
		software-properties-common \
		ssh \
		sudo \
		sysstat \
		tar \
		tcpdump \
		tmux \
		traceroute \
		unattended-upgrades \
		uuid-runtime \
		vim \
		wget

# Install a specific kernel
RUN apt-get -q update && \
	apt-get -y -qq upgrade && \
	apt-get -y -qq install \
	linux-image-4.15.0-45-generic \
	linux-modules-extra-4.15.0-45-generic \
	linux-headers-4.15.0-45-generic

# Configure locales
RUN locale-gen en_US.UTF-8 && \
    dpkg-reconfigure locales

# Fix permissions
RUN chown root:syslog /var/log \
	&& chmod 755 /etc/default

# Fix cloud-init EC2 warning
RUN touch /root/.cloud-warnings.skip

# vim: set tabstop=4 shiftwidth=4:

## HW specific image modifications go in this file
# ENV sdk_bin https://download.01.org/intel-sgx/linux-2.4/ubuntu18.04-server/sgx_linux_x64_sdk_2.4.100.48163.bin
ENV drv_bin https://download.01.org/intel-sgx/linux-2.4/ubuntu18.04-server/sgx_linux_x64_driver_778dd1f.bin
ENV psw_deb https://download.01.org/intel-sgx/linux-2.4/ubuntu18.04-server/libsgx-enclave-common_2.4.100.48163-bionic1_amd64.deb
# ENV psw_dev_deb https://download.01.org/intel-sgx/linux-2.4/ubuntu18.04-server/libsgx-enclave-common-dev_2.4.100.48163-bionic1_amd64.deb
# ENV psw_dbgsym_deb https://download.01.org/intel-sgx/linux-2.4/ubuntu18.04-server/libsgx-enclave-common-dbgsym_2.4.100.48163-bionic1_amd64.ddeb
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
 	apt-get install -y build-essential ocaml ocamlbuild automake autoconf libtool wget python libssl-dev libcurl4-openssl-dev protobuf-compiler libprotobuf-dev sudo kmod vim curl git-core libprotobuf-c0-dev libboost-thread-dev libboost-system-dev liblog4cpp5-dev libjsoncpp-dev alien uuid-dev libxml2-dev cmake pkg-config expect systemd-sysv
	
# Clean APT cache
RUN apt-get clean \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/log/*

# now for some Docker-specific tweaks

RUN set -xe \
	\
# https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L40-L48
	&& echo '#!/bin/sh' > /usr/sbin/policy-rc.d \
	&& echo 'exit 101' >> /usr/sbin/policy-rc.d \
	&& chmod +x /usr/sbin/policy-rc.d \
	\
# https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L54-L56
	&& dpkg-divert --local --rename --add /sbin/initctl \
	&& cp -a /usr/sbin/policy-rc.d /sbin/initctl \
	&& sed -i 's/^exit.*/exit 0/' /sbin/initctl

RUN mkdir /root/sgx && \
    mkdir /etc/init && \
	wget -O /root/sgx/drv.bin ${drv_bin} && \
    wget -O /root/sgx/psw.deb ${psw_deb} && \
    # wget -O /root/sgx/psw_dev.deb ${psw_dev_deb} && \
    # wget -O /root/sgx/psw_dbgsym.deb ${psw_dbgsym_deb} && \
    # wget -O /root/sgx/sdk.bin ${sdk_bin} && \
    cd /root/sgx && \
	chmod +x /root/sgx/drv.bin && \
    /root/sgx/drv.bin && \
    dpkg -i /root/sgx/psw.deb && \
    # dpkg -i /root/sgx/psw_dev.deb && \
    # dpkg -i /root/sgx/psw_dbgsym.deb && \
    # chmod +x /root/sgx/sdk.bin && \
    # echo -e 'no\n/opt' | /root/sgx/sdk.bin && \
    # echo 'source /opt/sgxsdk/environment' >> /root/.bashrc && \
    rm -rf /root/sgx/*

WORKDIR /root

